#!/bin/sh

workingBranch="$(git rev-parse --abbrev-ref HEAD)"

if [ "$(git status --porcelain=1 | wc -l)" -ne 0 ]; then
  echo "Unstaged changes or uncommited staged changes detected!" >&2
  echo "Please commit, stash or reset before deploying" >&2
  exit 1
fi

MD2HTML="pandoc -f gfm" shellscribe src build -v

if [ "$(git branch --list deploy)" ]; then
  git branch -D deploy
fi

git switch --orphan deploy

mv build/* build/.* .
rmdir build

git add .
git commit -m "deploy" --no-gpg-sign
git push origin deploy --force

git checkout "$workingBranch"
git reset --hard HEAD && git clean -fd
