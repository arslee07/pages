# Projects

Please note that this list may not be complete. Visit my [Codeberg](https://codeberg.org/arslee07), [SourceHut](https://git.sr.ht/~arslee07) and [GitHub](https://github.com/arslee07) for more stuff.

## Libraries

- [Rive](https://codeberg.org/rive/rive) - Rust ecosystem for Revolt.
- [anilibria.dart](https://github.com/arslee07/anilibria.dart) - Dart wrapper around [AniLibria](https://anilibria.tv) API.
- [gemini.dart](https://codeberg.org/arslee07/gemini.dart) - A set of Dart libraries for [Gemini](https://gemini.circumlunar.space/) protocol.
- [tgen](https://github.com/funny-pineapples/tgen) - Dead simple text generation library for Python.
- [micronbt](https://codeberg.org/arslee07/micronbt) - Tiny C99 NBT reader.
- [hare-nbt](https://git.sr.ht/~arslee07/hare-nbt) - NBT support for hare.

## Mobile

- [anilibria-flutter](https://codeberg.org/arslee07/anilibria-flutter) - Cross-platform AniLibria client.
- [Vostok](https://codeberg.org/arslee07/vostok) - Cross-platform Gemini browser.

## Web

- [pb](https://github.com/arslee07/pb) - Simple r/place clone.
- [Shellscribe](https://codeberg.org/arslee07/shellscribe/) - Simple static content generator written in POSIX Shell.
- [This website](https://codeberg.org/arslee07/pages) - My personal website.

## Other

- [FDL](https://codeberg.org/fdl) - Minecraft SMP and digital ecosystem for it (currently frozen).
- [SimpleLogin](https://codeberg.org/arslee07/SimpleLogin) - A pretty simple login plugin for Project Poseidon (Minecraft b1.7.3).
- [Pollify](https://github.com/ThePollify/) - Interactive polls for your presentations (unfinished prototype).

## Just For Fun™

- [Anonchat](https://github.com/anonchat-org) - Dumb messaging protocol.
- [ctbf](https://codeberg.org/arslee07/ctbf) - Compile Time BrainFuck. Made using Rust macros.
- [meow-kernel-module](https://codeberg.org/arslee07/meow-kernel-module) - A kernel module that creates a virtual device that just endlessly writes 'meow'.
- [1kb](https://codeberg.org/arslee07/1kb) - A small web page that links to this site, under a kilobyte with SVG and CSS transition.
