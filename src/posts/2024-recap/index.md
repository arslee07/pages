# My 2024 recap

Hi!

I didn't do much in 2024, mostly because I was focused on my studies. I'm going to university next year, so my activity won't change. My plan is to go to university in Central Russia and then settle there. There are no good prospects in the Far East, where I live now.

Anyway, I have something to share.

## Personal projects

This year most of my projects were written in C with a bit of Shell scripts, Make recipes and other pure spartan Unix stuff. Despite all the footguns I encountered, I still enjoyed programming using these tools.

[`ug`](https://codeberg.org/arslee07/ug) is a minimalist video game platform. Basically, it's a tiny interface for a game and a runtime. The runtime runs the game loop and implements a minimal set of functions, such as: reading input, drawing a pixel, playing a sound, loading or saving some data. The game consists of two function: initialization (runs only once) and update (runs every frame). `ug` has some hardware restrictions: 64x64 RGB332 display, 16 bits of ROM, 6 buttons and 4 available sounds. That was decided with embedded devices (like Arduino) in mind. I abandoned this project because I lost interest in it. But I also can say that the project is finished, since the core interface is done. :)

[Shellscribe](https://codeberg.org/arslee07/Shellscribe) is a static website generatkr written in POSIX Shell. The idea is pretty simple: render Markdown files to HTML and copy other files as-is. Shellscribe is made primarily for this website, but can be used for any website where tools like Jekyll or Hugo are overkill. New features are added as the website evolves.

I planned to open my own Minecraft beta 1.7.3 server, [`mc.arslee.me`](https://mc.arslee.me). The idea is to make all mods and plugins in-house. Well, the server is down now, but some stuff has been developed.

[SimpleLogin](https://codeberg.org/arslee07/SimpleLogin) is a tiny login plugin for offline mode servers running [Project Poseidon](https://github.com/retromcorg/Project-Poseidon) (a beta 1.7.3 CraftBukkit fork) that works out of the box.

[Mapka](https://codeberg.org/arslee07/Mapka) is an external Minecraft map renderer that aims to be fast and portable, and to support a wide range of versions, while maintaining the look and feel of the Minecraft map item. During development, auxiliary libraries were made.

[`micronbt`](https://codeberg.org/arslee07/micronbt) is a tiny Named Binary Tag parser written in C99 with a convenient API and the ability to provide your own byte stream to read (e.g. file or TCP socket).

[`piconbt`](https://codeberg.org/arslee07/piconbt) is a smaller alternative to `micronbt` that only supports static buffers, but does zero-copy and no dynamic memory allocation. Unlike `micronbt`, which loads and parses the entire tree at once, `piconbt` acts as an iterator over tags.

Unfortunately, I haven't contributed much to other people's projects this year. All I've done is: contributed to [Revolt](https://revolt.chat) translations, [Team Fortress 2 Classic](https://tf2classic.com) installer translations, minor style fixes in [BotiCord API Documentation](https://docs.boticord.top) and a minor tweak in [markdown.awk](https://git.sr.ht/~knazarov/markdown.awk).

Oh, I also joined [1kB Club](https://1kb.club) with [hi.arslee.me](https://hi.arslee.me) website.

## Programming contests

This year, I participated in many engineering and computer science contests.

In early February, I took part in [Cognitive Technologies](https://olymp.misis.ru), a computer science competition. I didn't do well because my skills in algorithms and data structures weren't strong enough, but it was still a great experience. The rules and organization were similar to ICPC, except participants competed alone.

At the end of February, the [National Technology Olympiad](https://ntcontest.ru) was held in the area of [Aquatic Robotics Systems](https://ntcontest.ru/tracks/nto-school/proekt-novogo-proizvodstva/vodnye-robototekhnicheskie-sistemy/). The Olympiad consists of two parts. The team stage involves solving a technical problem covering several skills at once: engineering design, electronics, microcontroller programming and computer vision. I did the latter. I wrote a program using OpenCV that scans the underwater space, detects complex objects and visualizes the data. The second stage, the individual part, is devoted to theoretical knowledge in computer science and physics. We didn't win the team part (we did it in 2023 though), but scored enough so I took the individual prize (as 60% of the team score contributed to individual rankings).

In March, I took part in NTO once again, but now in the area of [Flying Robotics](https://ntcontest.ru/tracks/nto-school/proekt-novogo-transporta/letayushchaya-robototekhnika/). The set of required skills remained the same, except that now, in addition to computer vision, I have to understand ROS and the features of the drone itself. We were just a little short of winning the team stage, but still managed to get our team members, who were unable to do so in February, to the individual prizes.

Between the previous two competitions, I participated in the [Rosatom Olympiad in Informatics](https://olymp.mephi.ru/rosatom_it), which was organized by MEPhI University. I had no special expectations, but I somehow became a prize winner.

In July, I went to the [Sirius](https://sochisirius.ru/o-siriuse/obschaja-informatsija) educational center. There, I was in a team doing a project called "Crawler robotic platform for working in Arctic conditions". In short, an autonomous tractor that drives in the snow. My role was the whole software part. This included developing the simulator (using Godot Engine), the control panel (using Flutter) and the firmware for the machine itself. The latter turned out to be the most complex, as I had to deal with the Frankenstein-like bundle of Raspberry Pi, Arduino and ESP32 that controls LoRa, WiFi, cameras, GPS, motors, etc.

In November, my team and I took part in the [ICPC Far East Qualifier](https://imcs.dvfu.ru/cats/rank_table?cache=1;cid=7413232;clist=7413232;hide_ooc=1;hide_virtual=1;sid=). We reached top 10 (out of 33 teams), but we didn't qualify because we weren't uni/college students yet. Not a bad score for school students anyway!

## Conclusion

There will be more to come.

See you in 2025!
